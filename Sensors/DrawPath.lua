local sensorInfo = {
	name = "DrawPath",
	desc = "Sends data to example debug widget",
	author = "Alatrist",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(points)
	local ID = 1
	for i=1, #points -1 do
			ID = ID+1
			if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					ID, -- key
					{	-- data
						startPos = points[i],
						endPos = points[i+1]
					}
				)
		end
	end
	return 	{1, -- key
				{	-- data
					startPos = 1,
					endPos = next(points)
				}
			}
end