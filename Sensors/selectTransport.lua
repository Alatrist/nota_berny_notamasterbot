local sensorInfo = {
	name = "SelectTransport",
	desc = "Select all transport units.",
	author = "Alatrist",
	date = "2019-04-29",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return filtered list of units
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @return newListOfUnits [array of unitIDS] filtered list
return function(listOfUnits)
	local newListOfUnits = {}
	
	for i=1, #listOfUnits do
		local thisUnitID = listOfUnits[i]
		local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
		if (UnitDefs[thisUnitDefID].isTransport) then -- in category
			newListOfUnits[#newListOfUnits + 1] = thisUnitID
		end 
	end
	
	return newListOfUnits
end