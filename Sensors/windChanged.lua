local sensorInfo = {
	name = "Windchanged",
	desc = "Ask if wind has changed significantly",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description return current wind statistics
return function(currentWind, lastWind, epsilonCosA)
	if (lastWind==nil) then
    return {
      changed=true,
      wind=currentWind
      }
  end
  local cosA = currentWind.normDirX* lastWind.wind.normDirX + currentWind.normDirZ* lastWind.wind.normDirZ
  if (1-cosA > epsilonCosA) then
    return {
      changed=true,
      wind=currentWind
      }
  end
  local newWind = lastWind.wind
  newWind.strength = currentWind.strength
  return {
    changed = false,
    wind = newWind
    
    }
end