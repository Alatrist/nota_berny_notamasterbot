local sensorInfo = {
	name = "Copy",
	desc = "Make new shallow copy of a table",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end


-- @description return current wind statistics
return function(original)
  local copy = {}
  for key, value in pairs(original) do
      copy[key] = value
  end
  return copy
end