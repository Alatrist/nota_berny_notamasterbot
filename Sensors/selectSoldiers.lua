local sensorInfo = {
	name = "Windchanged",
	desc = "Select only armed units",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description return current wind statistics
return function(data)
	output = {}
  for i=1, #data do
    weapons = UnitDefs[Spring.GetUnitDefID(data[i])].weapons
    if not (weapons == nil or #weapons ==0) then
      table.insert(output, data[i])
    end
  end
  return output
end