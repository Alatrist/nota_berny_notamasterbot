local sensorInfo = {
	name = "sortByDistanceTo",
	desc = "Sort units outside of an area",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end

function getPosition(unit)
  local x,y,z = Spring.GetUnitPosition(unit)
  return Vec3(x,y,z)
end

function createCompareFunc(point)
  return function(a, b)
   return getPosition(a):Distance(point) < getPosition(b):Distance(point)
  end
end

-- @description return current wind statistics
return function(data, point)
  local tab = {}
  for i = 1, #data do
    local x,y,z = Spring.GetUnitPosition(data[i])
    local pos = Vec3(x,y,z)
    if pos:Distance(point["center"])>point["radius"]/2 then
      table.insert(tab, data[i])
    end
  end
  table.sort(tab, createCompareFunc(point["center"]))
  return tab
end