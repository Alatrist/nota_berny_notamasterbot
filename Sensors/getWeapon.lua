local sensorInfo = {
	name = "getWeapon",
	desc = "Force end the mission",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end


-- @description return current wind statistics
return function(myId)
  local enemyId = Spring.GetUnitDefID(Spring.GetUnitNearestEnemy(myId))
  if enemyId ~= nil then
    local weapons = UnitDefs[enemyId].weapons
    if weapons ~=nil and #weapons > 0 then
      return {UnitDefs[enemyId].name, WeaponDefs[weapons[1].weaponDef].name, WeaponDefs[weapons[1].weaponDef].range}
    end
  end
end