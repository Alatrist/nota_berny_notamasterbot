local sensorInfo = {
	name = "canUnload",
	desc = "Ask if this place is empty",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end


-- @description return current wind statistics
return function(transporter, target)
  local x,y,z = Spring.GetUnitPosition(transporter)
  local unitsInC = Spring.GetUnitsInCylinder(x,z, 50)
  for i=1, #unitsInC do
    if unitsInC[i]~=transporter and unitsInC[i]~=target then
      return false
    end
  end
  return true
end