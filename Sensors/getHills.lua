local sensorInfo = {
	name = "getHills",
	desc = "Find hills",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function mapHill(current, hillHeight, hillNumb, epsilon)
  if math.abs(current.h - hillHeight) <= epsilon then
    current.hill = hillNumb
    return true
  elseif current.h > hillHeight then
    current.hill = -1
  end
  return false
end

function computeCentroid(hill)
  return { x = hill.sumX / hill.count , z = hill.sumZ / hill.count }
end

function hillNumbToHeight(hillNumb, hillTable)
  return hillTable[hillNumb].sumY / hillTable[hillNumb].count
end

-- @description return current wind statistics
return function()
hillsMap = {}
hills = {}
heightEpsilon = 60
stepSize = Game.squareSize/4
mapSizeX = Game.mapSizeX 
mapSizeZ = Game.mapSizeZ 
 
 table.insert(hills,{ sumX = 0, sumZ = 0, sumY = 0, count = 0})
for i = 1, mapSizeX/stepSize do
  hillsMap[i] = {}
  for j = 1, mapSizeZ/stepSize do
    x = i*stepSize
    z = j*stepSize
    h = Spring.GetGroundHeight(x, z)
    
    normalX, normalY, normalZ = Spring.GetGroundNormal(x,z)
    
    hillsMap[i][j] = { h = h, hill = 1}
    
    if normalX == 0 and normalZ == 0 then
  
      if i > 1 and j > 1 then
        nX, nY, nZ = Spring.GetGroundNormal(x,z)
        upperHillHeight = hillNumbToHeight(hillsMap[i-1][j].hill, hills)
        leftHillHeight = hillNumbToHeight(hillsMap[i][j-1].hill, hills)
        
        if not mapHill(hillsMap[i][j], upperHillHeight, hillsMap[i-1][j].hill, heightEpsilon) then
          mapHill(hillsMap[i][j], leftHillHeight, hillsMap[i][j-1].hill, heightEpsilon)
        end

        if hillsMap[i][j].hill == -1 then
          table.insert(hills,{ sumX = x, sumZ = z, sumY = h, count = 1})
          hillsMap[i][j].hill = #hills + 0
        else
          hill = hills[hillsMap[i][j].hill]
          hill.sumX = hill.sumX + x
          hill.sumZ = hill.sumZ + z
          hill.sumY = hill.sumY + h
          hill.count = hill.count + 1
        end
        
      else
          hill = hills[hillsMap[i][j].hill]
          hill.sumX = hill.sumX + x
          hill.sumZ = hill.sumZ + z
          hill.sumY = hill.sumY + h
          hill.count = hill.count + 1
      end
      
    end
  end

end
output = {}
for i = 2, #hills do
  table.insert(output,computeCentroid(hills[i]))
end

return output
end