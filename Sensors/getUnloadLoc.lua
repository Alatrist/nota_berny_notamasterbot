local sensorInfo = {
	name = "randomSafe",
	desc = "Try find empty place to unload an unit.",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end


-- @description return current wind statistics
return function(transporter, target, safeLoc)
  for j = 1, 10 do
    local dx = math.random(-safeLoc["radius"], safeLoc["radius"])
    local dz = math.random(-safeLoc["radius"], safeLoc["radius"])
    local loc = Vec3(safeLoc["center"].x+dx, y, safeLoc["center"].z+dz)
    local unitsInC = Spring.GetUnitsInCylinder(loc.x,loc.z, 25)
    local badPlace = false
    for i=1, #unitsInC do
      if unitsInC[i]~=transporter and unitsInC[i]~=target then
        badPlace=true
        break
      end
    end
    if not badPlace then
      return loc
    end
  end
  return nil
end