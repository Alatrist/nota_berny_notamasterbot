local sensorInfo = {
	name = "freeUnit",
	desc = "Delete reservation and remove unit from the list",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end


-- @description return current wind statistics
return function(unit, reservations, item, itemList)
	reservations[unit] = nil
  reservations[item] = nil
  for i=1, #itemList do
    if itemList[i] == item then
      itemList[i]=nil
      break
    end
  end
end