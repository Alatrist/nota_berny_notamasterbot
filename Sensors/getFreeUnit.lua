local sensorInfo = {
	name = "getFreeUnit",
	desc = "Get a nonreserved unit",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = -1 -- no caching
	}
end


-- @description return current wind statistics
return function(unitList, reservations)
	for i = 1, #unitList do
    if unitList[i] ~= nil and not Spring.GetUnitIsDead(unitList[i]) then
      if reservations[unitList[i]] == nil then
        return unitList[i]
      end
    end
  end
  return nil
end