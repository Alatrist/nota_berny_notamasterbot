local sensorInfo = {
	name = "GetRiverPath",
	desc = "",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end

Queue = {}
function Queue.new()
  return {first =0, last = -1}
end

function Queue.pushBack(queue, value)
  local last = queue.last + 1
  queue.last = last
  queue[last]=value
end

function Queue.pop(queue)
  local first = queue.first
  if first > queue.last then error("cannot pop from empty") end
  local value = queue[first]
  queue[first] = nil
  queue.first = first + 1
  return value
end

function Queue.empty(queue)
  return queue.first>queue.last
end


-- @description return current wind statistics
return function(grid, start, finish)
local maxX = Game.mapSizeX
local maxZ = Game.mapSizeZ

local minS = 9999999
local minSVal
local minF = 9999999
local minFVal


for keyPoint,value in pairs(grid)  do
  local distS = start:Distance(keyPoint)
  if distS < minS then
    minS = distS
    minSVal = keyPoint
  end
  local distF = finish:Distance(keyPoint)
  if distF < minF then
    minF = distF
    minFVal = keyPoint
  end
end


local graph = {}

local queue = Queue.new()
local visited = {}

local finished = false

Queue.pushBack(queue, minFVal)
visited[minFVal] = minFVal

while not Queue.empty(queue) do
  --pop
  current = Queue.pop(queue)

  
  if current == minSVal then 
    finished=true
    break
  end  
  
  local neig = grid[current]
  for i = 1, #neig do
    if visited[neig[i]] == nil then
      Queue.pushBack(queue, neig[i])
      graph[neig[i]] = current
      visited[neig[i]] = neig[i]
    end
  end
end

  
if finished then
  local path = {}
  local val = minSVal
  table.insert(path, minSVal)
  while val ~= minFVal do
    val = graph[val]
    table.insert(path, val)
  end
  return path
else
  return nil
end  
end