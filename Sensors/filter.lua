local sensorInfo = {
	name = "filterTransportable",
	desc = "Select transportable units which are not in the area",
	author = "Alatrist",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load


function getInfo()
	return {
		period = 0 -- no caching
	}
end


-- @description return current wind statistics
return function(inputList, safeArea)
  local outputlist = {}
  for i=1, #inputList do
    local defId = Spring.GetUnitDefID(inputList[i])
    local x,y,z = Spring.GetUnitPosition(inputList[i])
    local vec = Vec3(x,y,z)
    local center = Vec3(safeArea["center"].x , safeArea["center"].y, safeArea["center"].z)
    if UnitDefs[defId].humanName ~= "hammer" and UnitDefs[defId].humanName ~= "Hammer" and not UnitDefs[defId].cantBeTransported and vec:Distance(center) > safeArea["radius"] then
      table.insert(outputlist, inputList[i])
    end
  end
  return outputlist
end