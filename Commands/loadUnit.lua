function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load unit",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
  local transporter = parameter.transporter 
  local target = parameter.target
	local cmdID = CMD.LOAD_UNITS
  
  local x,y,z = SpringGetUnitPosition(transporter)
  local xt,yt,zt = SpringGetUnitPosition(target)
  local myP = Vec3(x,yt,z)
  local tP = Vec3(xt,yt,zt)
  
  if yt==nil or yt<0 then
    return FAILURE
  end

  local targetId = Spring.GetUnitDefID(target)
  
  local currentTr = Spring.GetUnitTransporter(target)
  
  if currentTr == nil then
    SpringGiveOrderToUnit(transporter, cmdID, {target}, {})
    return RUNNING
  elseif currentTr ~= transporter then
    return FAILURE
  else
    return SUCCESS
  end
end



function Reset(self)
	ClearState(self)
end
