function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "",
		parameterDefs = {
			{ 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "hills",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local listOfUnits = parameter.listOfUnits -- table
	local hills = parameter.hills -- table

	local cmdID = CMD.MOVE

  notDone = false
  local min = math.min(#hills, #listOfUnits)
  for i=1, min do
    local pointX, pointY, pointZ = SpringGetUnitPosition(listOfUnits[i])
    local unitPosition = Vec3(pointX, pointY, pointZ)
    local hillVec = Vec3(hills[i].x, pointY, hills[i].z)
    if not Spring.GetUnitIsDead(listOfUnits[i]) then
      if unitPosition:Distance(hillVec) > 10 then
        notDone= true
      end
      SpringGiveOrderToUnit(listOfUnits[i], cmdID, {hills[i].x, pointY, hills[i].z }, {})
    end
	end	
  if notDone then
		return RUNNING
  else
    return SUCCESS
  end
end


function Reset(self)
	ClearState(self)
end
