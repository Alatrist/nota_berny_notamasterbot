function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move safely to the target",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local transporter = parameter.transporter 
  local target = parameter.target
  if Spring.GetUnitIsDead(transporter) or not Spring.ValidUnitID(transporter) then
    return FAILURE
  end
  local x,y,z= SpringGetUnitPosition(transporter)
  local currentLoc = Vec3(x,y,z)
  local targetLoc = Vec3(target.x, target.y, target.z)
  local gHeight = Spring.GetGroundHeight(x, z)
  local unitHeight = y-gHeight
  target.x = math.max(target.x, 0)
  target.z = math.max(target.z,0)
  
  if currentLoc:Distance(targetLoc) > 50 + currentLoc.y-targetLoc.y then
    local nearestEn = Spring.GetUnitNearestEnemy(transporter)
    local newLoc
    local goodLoc = true
    
    -- Move around range areas
    if nearestEn ~= nil and currentLoc:Distance(targetLoc) > 700 then
      local enemyID = Spring.GetUnitTeam(nearestEn)
      local enemies = Spring.GetUnitsInCylinder(x,z, 1000, enemyID)
      if enemies ~= nil and #enemies > 0 then  
        local defaultDir = (targetLoc - currentLoc):GetNormal();
        --select safe direction
        for d = 0, 12 do
          local sig = d - math.floor(d/2) *2
          sig = sig * -1
          local dir = defaultDir:Rotate2D( sig*360/ 6 * d/2)
          dir:Mul(300)
          goodLoc = true
          for i = 1, #enemies do
            local enemyId = Spring.GetUnitDefID(enemies[i])
            local ex,ey,ez = Spring.GetUnitPosition(enemies[i])
            local enemyPos = Vec3(ex,ey,ez)
            local range = 700
            if enemyId == nil then
              range = 700
            else
              local weaponId = UnitDefs[enemyId].weapons[1].weaponDef
              range = WeaponDefs[weaponId].range
            end           
            newLoc = currentLoc + dir
            local newY = Spring.GetGroundHeight(newLoc.x, newLoc.z) + unitHeight
            newLoc.y = newY
            if enemyPos:Distance(newLoc) < range +200 and newY-enemyPos.y > -300 then 
              goodLoc = false
              break
            end
          end
          if goodLoc then
            break
          end
        end
        SpringGiveOrderToUnit(transporter, CMD.MOVE, {newLoc.x, newLoc.y, newLoc.z}, {})
        return RUNNING
      end
    end
    
    -- Select the closest path
      local velx, vely, velz, velLength = Spring.GetUnitVelocity(transporter)
      if velLength == nil then
        return FAILURE
      end
        SpringGiveOrderToUnit(transporter, CMD.MOVE, {targetLoc.x, targetLoc.y, targetLoc.z}, {})
      return RUNNING
    end
  return SUCCESS

end


function Reset(self)
	ClearState(self)
end
