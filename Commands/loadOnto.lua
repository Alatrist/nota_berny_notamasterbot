function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local transporter = parameter.transporter 
  local listOfUnits = parameter.listOfUnits
	local cmdID = CMD.LOAD_ONTO
  
  local loading = false
  for i = 1, #listOfUnits do
    id = Spring.GetUnitDefID(listOfUnits[i])
    if listOfUnits[i] ~= transporter and not UnitDefs[id].cantBeTransported then
      if Spring.GetUnitTransporter(listOfUnits[i]) == nil then
        loading=true
        SpringGiveOrderToUnit(listOfUnits[i], cmdID, {transporter}, {})
      end
    end
  end
  if loading then 
    return RUNNING
  else
    return SUCCESS
  end 

end


function Reset(self)
	ClearState(self)
end
