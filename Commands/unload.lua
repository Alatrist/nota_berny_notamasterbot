function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "listOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "position",
				variableType = "Position",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local transporter = parameter.transporter 
  local listOfUnits = parameter.listOfUnits
  local position = parameter.position
	local cmdID = CMD.UNLOAD_UNIT
  
  local unloading = false
  for i = 1, #listOfUnits do
    if listOfUnits[i] ~= transporter and not UnitDefs[Spring.GetUnitDefID(listOfUnits[i])].cantBeTransported then
      if Spring.GetUnitTransporter(listOfUnits[i]) == transporter then
        unloading=true
        SpringGiveOrderToUnit(transporter, cmdID, {position.x, position.y, position.z}, {})
      end
    end
  end
  if unloading then 
    return RUNNING
  else
    return SUCCESS
  end 

end


function Reset(self)
	ClearState(self)
end
