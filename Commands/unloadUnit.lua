function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
  local transporter = parameter.transporter 
  local target = parameter.target
	local cmdID = CMD.UNLOAD_UNIT
  
  local targetId = Spring.GetUnitDefID(target)
  local currentTr = Spring.GetUnitTransporter(target)
  
  if currentTr == transporter then
    local x, y, z = Spring.GetUnitPosition(transporter)
    SpringGiveOrderToUnit(transporter, cmdID, {x,y,z}, {})
    return RUNNING
  else
    return SUCCESS
  end
end



function Reset(self)
	ClearState(self)
end
