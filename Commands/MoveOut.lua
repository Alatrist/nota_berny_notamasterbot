function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move out of an area",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "area",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
  local unit = parameter.unit 
  local area = parameter.area
  local x,y,z = SpringGetUnitPosition(unit)
  local pos = Vec3(x,y,z)
  local areaPos = Vec3(area.center.x, area.center.y, area.center.z)
  
  if pos:Distance(areaPos) > area.radius then
    return SUCCESS
  end
  
  local dist = area.radius + 500
  SpringGiveOrderToUnit(unit, CMD.MOVE, {areaPos.x, areaPos.y, areaPos.z + dist}, {})
  return RUNNING
end



function Reset(self)
	ClearState(self)
end
