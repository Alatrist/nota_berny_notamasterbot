function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move safely to the target",
		parameterDefs = {
			{ 
				name = "transporter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "dir",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local transporter = parameter.transporter 
  local path = parameter.path
  local dir = parameter.dir
  if Spring.GetUnitIsDead(transporter) then
    return FAILURE
  end
  local x,y,z = SpringGetUnitPosition(transporter)
  local pos = Vec3(x,y,z)
  
  local target 
  if dir then
    target = path[#path]
  else
    target = path[1]
  end

  if pos:Distance(target) < 200 then
    return SUCCESS
  end
 local queue = Spring.GetUnitCommands(transporter, 100)
  if queue == nil or #queue==0 then
    if dir then
      for i = 1, #path do
            SpringGiveOrderToUnit(transporter, CMD.MOVE, {path[i].x, path[i].y, path[i].z}, {"shift"})
      end
    else
       for i = #path, 1, -1 do
            SpringGiveOrderToUnit(transporter, CMD.MOVE, {path[i].x, path[i].y, path[i].z}, {"shift"})
      end
    end
end

  return RUNNING
  
end


function Reset(self)
	ClearState(self)
end
