# Nota AI

* [Dependencies](https://bitbucket.org/Alatrist/nota_berny_notamasterbot/src/master/dependecies.json)

## License
* windWalker logo is derivative of [Font Awesome 5 solid wind.svg](https://commons.wikimedia.org/wiki/File:Font_Awesome_5_solid_wind.svg) by [Font Awesome](https://fontawesome.com/), used under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.en) 
* rescueTeam logo is derivative of [transportation symbol air aerial](https://svgsilh.com/image/310647.html) by [svgsilh.com](https://svgsilh.com), used under [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.en) 


